package com.bolsadeideas.springboot.web.app.controller;

import com.bolsadeideas.springboot.web.app.model.Usuario;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/app")
public class IndexController {
    @Value("${texto.indexcontroller.index.titulo}")
    private String textoIndex;
    @Value("${texto.indexcontroller.perfil.titulo}")
    private String textoPerfil;
    @Value("${texto.indexcontroller.listar.titulo}")
    private String textoListar;

    @GetMapping(value = {"/index","/","/home"})
    public String index(ModelMap model){
        model.addAttribute("titulo",textoIndex);
        return "index";
    }

    @RequestMapping("/perfil")
    public String perfil(Model model){
        Usuario usuario = new Usuario();
        usuario.setNombre("Fermin");
        usuario.setApellido("Jasso");
        usuario.setEmail("ferminfloresjasso@hotmail.com");
        model.addAttribute("usuario",usuario);
        model.addAttribute("titulo",textoPerfil.concat(usuario.getNombre()));
        return "perfil";
    }

    @RequestMapping("/listar")
    public String listar(Model model){
        model.addAttribute("titulo",textoListar);
        return "listar";
    }

    //Se recomienda si es que vamos a utilizar estos datos en mas de un metodo handler.
   @ModelAttribute("usuarios")
    public List<Usuario> poblarUsuarios(){
       List<Usuario> usuarios = Arrays.asList(
               new Usuario("Fermin","Jasso","ferminfloresjasso@hotmail.com"),
               new Usuario("Tommy","Flores","tommyFlores@intercam.com.mx"),
               new Usuario("Jakie","Flores","jakie@gmail.com")
       );
       return usuarios;
    }

}
